import java.util.Collection;
import java.util.*;
import java.io.IOException;

public class IMDBGraphImpl implements IMDBGraph {
	
	Scanner actorsScanner;
	Scanner actressesScanner;
	List<Actor> actors;
	List<Movie> movies;
	private int _actorLines;
	private int _actressLines;
	
	
	public IMDBGraphImpl(String string, String string2) throws IOException{
		this.actorsScanner = new Scanner(string);
		this.actressesScanner = new Scanner(string2);
		this.actors = new ArrayList<Actor>();
		this.movies = new ArrayList<Movie>();
		this._actorLines = 240;
		this._actressLines = 242;
	}

	@Override
	public Collection<? extends Node> getActors() {
		if(movies.isEmpty()) {
			//Add all the male actors to actors
			addMaleActorsToActors();
			//Add all the female actors to actors
			addFemaleActorsToActors();
		} else if(actors.isEmpty()) {
			//Add all the actors from the movies
			for(Movie m : movies)
				actors.addAll(m.getActors());
		}
		
		
		return this.actors;
	}
	
	//Adds all the female actors to the actor list of this class
	private void addFemaleActorsToActors() {
		for(int i = 0; i < this._actressLines; i++)
			this.actressesScanner.nextLine();
		
		while(this.actressesScanner.hasNextLine()) {
			Actor currentActor;
			int amountOfActors = 0;
			String movie = "";
			String line = actressesScanner.nextLine();
			String[] data = line.split("\t");
			
			for(int i = 0; i < data.length; i++) {
				if(!data[i].equals("")) {
					amountOfActors++;
					currentActor = new Actor(data[i], new ArrayList<Movie>());
					actors.add(currentActor);
				} else if(data.length > 1){
					actors.get(amountOfActors).addMovie(new Movie(data[i+1], new ArrayList<Actor>()));
				}
			}
			
		}
		
	}

	//Adds all the male actors to the actor list of this class
	private void addMaleActorsToActors() {
		for(int i = 0; i < this._actorLines; i++)
			this.actorsScanner.nextLine();
		
		while(this.actorsScanner.hasNextLine()) {
			Actor currentActor = new Actor(null, new ArrayList<Movie>());
			int amountOfActors = 0;
			String movie = "";
			String line = actorsScanner.nextLine();
			String[] data = line.split("\t");
			
			for(int i = 0; i < data.length; i++) {
				if(!data[i].equals("")) {
					amountOfActors++;
					currentActor.setName(data[i]);
					actors.add(currentActor);
				} else if(data.length > 1){
					Movie m = new Movie(data[i+1], new ArrayList<Actor>());
					m.addActor(currentActor);
					actors.get(amountOfActors).addMovie(m);
				}
			}
			
		}
	}
	
	@Override
	public Collection<? extends Node> getMovies() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Node getMovie(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Node getActor(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
