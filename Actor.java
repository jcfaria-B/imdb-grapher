import java.util.*;

public class Actor implements Node{
	
	private String _name;
	private List<Movie> _movies;
	
	public Actor(String name, ArrayList<Movie> movies) {
		this._name = name;
		this._movies = movies;
	}

	@Override
	public String getName() {
		return this._name;
	}

	@Override
	public Collection<? extends Node> getNeighbors() {
		return (Collection<? extends Node>) this._movies;
	}
	
	public void addMovie(Movie m) {
		this._movies.add(m);
	}
	
	public void setName(String s) {
		this._name = s;
	}
}
