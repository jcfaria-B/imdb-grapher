import java.util.*;
import java.util.Collection;

public class Movie implements Node{
	
	private String _name;
	private List<Actor> _actors;
	
	public Movie(String name, ArrayList<Actor> actors) {
		this._name = name;
		this._actors = actors;
	}

	@Override
	public String getName() {
		return this._name;
	}

	@Override
	public Collection<? extends Node> getNeighbors() {
		return (Collection<? extends Node>) this._actors;
	}
	
	public void addActor(Actor a) {
		this._actors.add(a);
	}
	
	public List<Actor> getActors(){
		return this._actors;
	}
	
}
